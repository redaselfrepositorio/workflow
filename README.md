Workflow
====================

>Criamos uma metodologia modificada e adptada de outras fontes já existentes.
>O nosso workflow usado para orientar a equipe no processo de desenvolvimento em equipe, seguindo padrões 
para alcançarmos agilidade e qualidade.




Ferramentas
---------------------

https://trello.com/   Gestão 

https://bitbucket.org/   Versionamento 

Algumas linguagem de programação inicialmente usada no desenvolvimento de mini projetos do zero:
javascript - php  -  html/css - jquery

https://br.wordpress.org/   Framework - Usado apenas para blogs ou divulgação de conteúdo para ontem.


Frameworks CSS
---------------------
http://getbootstrap.com/                         BOOTSTRAP

http://materializecss.com/                       MATERIALIZE

http://opensource.locaweb.com.br/locawebstyle/   LOCAWEBSTYLE




Trello
====================
>Criamos um quadro GESTÃO DESENVOLVIMENTO.

Há 2 boards essenciais o board principal é o ´TRIAGEM subprojetos´ onde cada card 
representa um projeto e pecorre as listas do Board. Desde o primeiro contato com o cliente no card clientes. 
Passar por card proposta, card Planejamento até se tornar realmente um projeto 
na lista card subprojetos. 

### BOARD triagem subprojetos

O card na lista subprojetos deve conter o titulo do projeto que deve ser apenas uma palavra e 
ter os membros responsáveis pelo projeto e a data do inicio além de um checklist 
que deve conter os passos desenvolvidos durante o planejamento e na sua 
descrição deve ter informações do cliente, telefone, email e a descrição do projeto.

### BOARD #project

O segundo board é onde as Tarefas definidas no planejamento serão desenvolvidas pelo time. O board deve ter o titulo igual o do card 
em project precedido de uma # assim eles são agrupados no trello e tem uma correspondência direta com o canal no slack, caso 
se escolhermos usar o slack.





Estrutura interna do board
====================

 1.lista de funcionalidades 		     `Tasks que serão entregues naquele sprint`
 
 2.Hold On		                         `Tasks que aguardam algum obstaculo para serem desenvolvidas`
 
 3.Developing                             `Implementando as tasks`
 
 4.Review Code                           `Review manual e automatizado`
 
 5.Production                            `Task já em produção`
 
 6.Master                                  `Tasks já incorporadas no branch master`
 




Cards
===================

>O padrão de cards esta intrissicamente ligado a os commits no repositório, temos uma filosofia "um card, um commit", porém no 
>inicio do projeto há bastante atividades que não são ligadas a desenvolvimento. Esses cards devem seguir o mesmo padrão, toda 
>atividade independente da area que pertence é uma task para o projeto, assim temos Task, Refact, Issue.


>>### Task

>>> A maior partes das atividades são tasks, são criadas a partir do refinamento do planejamento ( aqui vai depender da metologia usada de desenvolvimento, exemplo scrum, xp e entre outros). O titulo deve ser a descrição da tarefa em uma linha precedida por Task00: numeração não reflete necessariamente a priorização das Tasks.

>>### Refact

>>>São Tasks que tem por objetivo uma mudança no material, sintaxe ou lógica mais que não altere os resultados da tarefa. O formato do titulo é o mesmo das Tasks mas precedido por Refact00:.

>>### Issue

>>>Os issues são incidências de erros após a conclusão de uma Task, reportadas pelo cliente ou durante a revisão do código. O formato do titulo é o mesmo das anteriores só que precedidos por Issue00:, porém a numeração é atribuida de acordo com a Task que contêm os erros.


Bitbucket
====================

Versionamento dos projetos.

>>### Repositórios

>>>O titulo do projeto deve ter o mesmo nome do card em project de TRIAGEM mini projetos e possuir um README.md com as instruções para iniciar um projeto, ferramentas utilizadas, configurações de ambiente e links para documentação, downloads e etc.

>>>>### Branchs

>>>>>Cada funcionalidade, refact ou problema corrigido de ser entregue via pull request. Assim o branch deve ter o mesmo titulo do card no Done, porém sem a descrição. Todo projeto tem três branchs principais: Master, Production e Review.

>>>>### review

>>>>>Este é o branch submetido a o code review, garantindo que não haja código sem uso, repetido, complexo e fora de estilo. Apenas código com nota A irão para a produção.

>>>>### Production


>>>>>Quando se trabalha com continuos o ultimo teste é em produção.

>>>>### master

>>>>>Este branch é o branch responsável pelo código estável do projeto, este repositório se destina a o código aprovado pelo cliente.

>### Commits

>> A mensagem deve seguir o seguinte formato sendo atribuido em cada parte as mesmas informações do card que deu origem aquele projeto.

>> **Tipo00: Titulo da task, issue ou refact**

>> Uma descrição mais detalhada, se necessaria. Quebra de linha
>> perto de 72 caracteres com uma linha vazia entre o titulo e a
>> descrição que devera descrever as alteração nos arquivos.

>> Caso seja necessario separe cada paragrafo por uma linha vazia
>> para manter a organização das ideias.